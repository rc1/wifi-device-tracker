var superagent = require('superagent');
var _ = require('underscore');

// # Responses
function sendOk (res) {
    res.send(200);
}

function sendError (res, message) {
    res.status(400).json({
        status: "error",
        message: message
    });
}

// # Classes

var DeviceIdCounter = 0;

function Device(options) {
    this.id = DeviceIdCounter++;
    this.host = options.host || "http://0.0.0.0:3000/";
    this.data = "";
}

Device.prototype.update = function (callback) {
    var self = this;
    superagent
        .get(this.host)
        .end(function (err, res) {
            if (!err) {
                self.data = res.text;
            }
            callback(err);
        });
};

function Tracker(app) {
    var self = this;

    this.devices = [];

    app.get('/api/device/', function(req, res, next) {
        res.json(getDevicesToSend());
    });

    app.post('/api/device/',
        // validate
        function (req, res, next) {
            if (typeof req.body.host !== 'string') {
                sendError(res, "missing host parameter");
            } else {
                next();
            }
        },
        // create
        function (req, res, next) {
            self.devices.push(new Device({
                host : req.body.host
            }));
            sendOk(res);
        });

    app.get('/api/device/:id/', function(req, res, next) {
        var device = deviceWithID(self.devices, req.params.id);
        if (!device) {
            sendError(res, "Could not find device with id of " + req.params.id);
        } else {
            device.update(function () {
                res.json(device);
            });
        }
    });

    function getDevicesToSend() {
        var deviceList = [];
        self.devices.forEach(function (device) {
            deviceList.push({
                id: device.id,
                host: device.host
            });
        });
        return deviceList;
    }

}

function deviceWithID(devices, id) {
    return _.find(devices, function (device) {
        return device.id == id;
    });
}


// # Exports

module.exports = {
    Tracker : Tracker,
    Device : Device
};