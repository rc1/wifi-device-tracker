function parse(input) {
    var stations = [];

    // input.match( /((.+\n?)+(?=\/100\n))+/m ).forEach(function (match) {
    //     console.log("-------------------");
    //     console.log("match = ", match);
    // });

    var addressLineMatches = input.match(/Address:.*/g);
    var essidLineMatches = input.match(/ESSID:.*/g);
    var quailyLineMatches = input.match(/Quality=.*/g);

    if (!addressLineMatches || !essidLineMatches || !quailyLineMatches) {
        console.log("failed to match any with", input);
         return stations;
    }
    
    var addresses = [];
    var essids = [];
    var quailities = [];
    var signals = [];

    addressLineMatches.forEach(function (addressLine, i) {
        addresses.push( addressLine.substring(9, addressLine.length) );
    });

    essidLineMatches.forEach(function (essidLine, i) {
         essids.push( essidLine.substring(7, essidLine.length).substring(0, essidLine.length - 1) );
    });

    quailyLineMatches.forEach(function (quailyLineMatches, i) {
        var outputLineMatched = quailyLineMatches.match(/[0-9\/db]+/g);

        quailities.push( outputLineMatched[0] );
        signals.push( outputLineMatched[1] );
    });

    for (var i=0; i<addresses.length; ++i) {
        stations.push({
            mac: addresses[i],
            essid: essids[i],
            quality: quailities[i],
            signalLevel: signals[i]
        });
    }

    return stations;
}

// Export
module.exports = parse;
