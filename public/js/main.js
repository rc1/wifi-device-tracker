$(function () {

    $(".addDevice").on('click', function () {
        superagent
            .post('/api/device/')
            .send({ host: $('input.host').val() })
            .end(function(res){
                updateDeviceList();
            });
    });

    $(".deviceList").on('click', '.device',  function () {
        console.log("clicked");
        var id = $(this).data('id');
        superagent
            .get('/api/device/'+id+'/')
            .end(function(res){
                $(".deviceDetails .container").text(res.body.data);
            });
    });

    updateDeviceList();

});

function updateDeviceList() {
    superagent
        .get('/api/device/')
        .end(function(res){
            renderDeviceList(res.body);
        });
}

function renderDeviceList(devices) {
    $('.deviceList').empty();
    devices.forEach(function (device) {
        $('.deviceList').append("<li class='device command' data-id='"+device.id+"'>"+device.id+": "+ device.host+ "</li>");
    });
}