SRC= ./src/iwlist-parser.js
TEST= ./test/iwlist-parser.test.js

all: test

test: SRC TEST
	mocha -R doc | cat docs/head.html - docs/tail.html > docs/test.html

.PHONY: test