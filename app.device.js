// the command line stuff
var span = require('child_process').spawn;
var iwlistParser = require('./lib/iwlist-parser');
var argv = require('optimist').argv;
var superagent = require('superagent');

var apiUrl = argv.a || argv.api || "http://wifidevicetracker.workers.io/api/";
var pollInterval = argv.i || argv.interval || 1000;
var uid = argv.u;

var lastOutput = null;

function pollForDifference() {
    fetchWifiDevices(function (output) {
        // Update the server with the new info
        if (!lastOutput || lastOutput !== output) {
            
            var devices = iwlistParser(output);
            superagent
                .post(apiUrl)
                .data(devices)
                .end(function (error) {
                    if (error) {
                        if (err) {
                            console.log(err);
                        } 
                        // Keep trying to send to server
                        else {
                            lastOutput = output;
                        }
                        setTimeout(pollForDifference, pollInterval);
                    } 
                });
        } 
        // Schedule another chack
        else {
             setTimeout(pollForDifference, pollInterval);
        }
    });
}

function fetchWifiDeviceInfo(callback) {
    var wifiProcess = span('iwlist', ['scan']);
    var output = "";
    wifiProcess.stdout.on('data', function(data) {
        output += data.toString();
    });
    wifiProcess.on('exit', function(code) {
       callback(output);
    });
}

function getUid(callback) {
    var uidProcess = span('hostid');
    var output = "";
    wifiProcess.stdout.on('data', function(data) {
        output += data.toString();
    });
    wifiProcess.on('exit', function(code) {
        uid = output;
        console.log("Setting uid to", uid);
        callback();
    });
} 

// Run
if (!uid) {
    getUid(pollForDifference);
} else {
    pollForDifference();
}
