var chai = (typeof chai === 'undefined') ? require('chai') : chai;
var expect = chai.expect;

var fs = require('fs');
var path = require('path');

var iwlistParser = require('./../lib/iwlist-parser');

describe('iwlist-parser', function () {

    describe('Required', function () {

        it('should be a function', function () {
            expect( iwlistParser ).to.be.a('function');
        });

    });

    var iwlistOutput = fs.readFileSync( path.join(__dirname, 'iwlist_output.txt') ).toString();

    describe('Loaded test file', function () {

            it('should be valid', function () {
                expect( iwlistOutput ).to.be.ok;
            });

            it('should have a big length', function () {
                expect( iwlistOutput ).length.to.be.above(10);
            });

        });


    describe('Parsing', function () {

        var results = iwlistParser( iwlistOutput );

        it('results are array', function () {
            expect( results ).length.to.be.above(0);
        });

        describe('first result is value', function () {

            it('results should have an essid', function () {
                for (var i=0; i<results.length; ++i) {
                    expect(results[i].essid).to.be.a('string');
                    expect(results[i].essid).length.to.be.above(1);
                }
            });

            it('results should have an mac', function () {
                for (var i=0; i<results.length; ++i) {
                    expect(results[i].mac).to.be.a('string');
                    expect(results[i].mac).length.to.be.above(1);
                }
            });

            it('results should have an quality', function () {
                for (var i=0; i<results.length; ++i) {
                    expect(results[i].quality).to.be.a('string');
                    expect(results[i].quality).length.to.be.above(1);
                }
            });

            it('results should have an signalLevel', function () {
                for (var i=0; i<results.length; ++i) {
                    expect(results[i].signalLevel).to.be.a('string');
                    expect(results[i].signalLevel).length.to.be.above(1);
                }
            });

        });

    });



});